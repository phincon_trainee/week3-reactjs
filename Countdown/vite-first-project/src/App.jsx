import './App.css'
import Countdown from './Countdown/Countdown'

function App() {

  // const endDate = new Date().getTime() + 365 * 24 * 60 * 60 * 1000;
  const endDate = new Date('2024-07-17T00:00:00').getTime();

  return (
    <div className='app-wrapper'>
      <Countdown heading="WE'RE LAUNCHING SOON" endDate={endDate} />
    </div>
  )
}

export default App
