import React, { useState, useEffect } from 'react';
import './Countdown.css';
// import './mobile.css';
import hills from '../assets/images/pattern-hills.svg';
import stars from '../assets/images/bg-stars.svg'
import facebook from '../assets/images/icon-facebook.svg'
import instagram from '../assets/images/icon-instagram.svg'
import pinterest from '../assets/images/icon-pinterest.svg'

// eslint-disable-next-line react/prop-types
const Countdown = ({ heading, endDate }) => {
    const [countdown, setCountdown] = useState({
        days: '00',
        hours: '00',
        minutes: '00',
        seconds: '00'
    });

    useEffect(() => {
        const interval = setInterval(() => {
            const now = new Date().getTime();
            const duration =
                typeof endDate === 'number'
                    ? endDate - now
                    // eslint-disable-next-line react/prop-types
                    : endDate.getTime() - now;

            const days = String(Math.floor(duration / (1000 * 60 * 60 * 24))).padStart(2, '0');
            const hours = String(Math.floor((duration % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).padStart(2, '0');
            const minutes = String(Math.floor((duration % (1000 * 60 * 60)) / (1000 * 60))).padStart(2, '0');
            const seconds = String(Math.floor((duration % (1000 * 60)) / 1000)).padStart(2, '0');

            setCountdown({ days, hours, minutes, seconds });
        }, 1000);

        return () => clearInterval(interval);
    }, [endDate]);

    return (
        <React.Fragment>
            <body>
                <div className="container">
                    <h3 className="heading">{heading}</h3>
                    <div className="countdown-wrapper">
                        <div className="countdown-value">
                            <h2>{countdown.days}</h2>
                            <p>{Number(countdown.days) > 1 ? 'Days' : 'Day'}</p>
                        </div>

                        <div className="countdown-value">
                            <h2>{countdown.hours}</h2>
                            <p>{Number(countdown.hours) > 1 ? 'Hours' : 'Hour'}</p>
                        </div>

                        <div className="countdown-value">
                            <h2>{countdown.minutes}</h2>
                            <p>{Number(countdown.minutes) > 1 ? 'Minutes' : 'Minute'}</p>
                        </div>

                        <div className="countdown-value">
                            <h2>{countdown.seconds}</h2>
                            <p>{Number(countdown.seconds) > 1 ? 'Seconds' : 'Second'}</p>
                        </div>
                        <div className='hills'></div>
                        <div className='image-container'>
                            <img src={hills} alt="gunung" />
                            <img src={stars} alt='bintang' />
                        </div>
                    </div>
                    <div className="footer">
                        <img src={facebook} />
                        <img src={instagram} />
                        <img src={pinterest} />
                    </div>
                    <div className="until">
                        <h5>Until : 2024-07-17T00:00:00 </h5>
                    </div>
                </div>
            </body>
        </React.Fragment>
    );
};

export default Countdown;
