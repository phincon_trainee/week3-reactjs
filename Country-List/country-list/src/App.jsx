import "./App.css";
import { Routes, Route } from "react-router-dom";
import AllCountries from "./component/AllCountries/AllCountries";
import CountryInfo from "./component/CountryInfo/CountryInfo";

import icon from "./assets/Vector.png";
import { useState } from "react";
function App() {
  const [theme, setTheme] = useState("light");
  const toggleTheme = () => {
    const body = document.querySelector("body");
    if (theme === "light") {
      setTheme("dark");
      body.classList.remove("light");
      body.classList.add("dark");
    } else {
      setTheme("light");
      body.classList.remove("dark");
      body.classList.add("light");
    }
  };
  return (
    <>
      <div className={`header ${theme}`} style={{ marginTop: "0" }}>
        <div className="container">
          <div className={`container_text ${theme}`}>
            <h5>Where in the world</h5>
            <p onClick={toggleTheme}>
              <img src={icon} alt="icon" />
              Dark Mode
            </p>
          </div>
        </div>
      </div>
      <div className={`container ${theme}`}>
        <Routes>
          <Route path="/" element={<AllCountries theme={theme} />} />
          <Route
            path="/country/:countryName"
            element={<CountryInfo theme={theme} />}
          />
        </Routes>
      </div>
    </>
  );
}
export default App;
