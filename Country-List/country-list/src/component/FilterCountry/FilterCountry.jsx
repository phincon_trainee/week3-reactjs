// eslint-disable-next-line no-unused-vars
import React from 'react';
import '../../App.css';

// eslint-disable-next-line react/prop-types
const FilterCountry = ({ onSelect, theme }) => {
    const selectHandler = (e) => {
        const regionName = e.target.value;
        onSelect(regionName);
    };
    return (
        <select onChange={selectHandler}>
            <option className={`option ${theme}`}>Filter by Region</option>
            <option className='option' value="Africa">
                Africa
            </option>
            <option className='option' value="America">
                America
            </option>
            <option className='option' value="Asia">
                Asia
            </option>
            <option className='option' value="Europe">
                Europe
            </option>
            <option className='option' value="Oceania">
                Oceania
            </option>
        </select>
    );
};
export default FilterCountry;