// eslint-disable-next-line no-unused-vars
import React, { useState, useEffect } from "react";
import "../../App.css";
import { apiURL } from "../util/api";
import SearchInput from "../Search/SearchInput1";
import FilterCountry from "../FilterCountry/FilterCountry";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { Pagination } from "@mui/material";
// import SortByAlphaIcon from "@mui/icons-material/SortByAlpha";

const AllCountries = ({ theme }) => {
  const [countries, setCountries] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState("");
  const [sortOrder, setSortOrder] = useState("asc");
  const [page, setPage] = useState(1); // Track the current page
  const countriesPerPage = 12; // Number of countries to display per page

  const getAllCountries = async () => {
    try {
      const res = await fetch(`${apiURL}/all`);
      if (!res.ok) throw new Error("Something went wrong !");
      const data = await res.json();
      console.log(data);
      setCountries(data);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError(error.message);
    }
  };

  const getCountryByName = async (countryName) => {
    try {
      const res = await fetch(`${apiURL}/name/${countryName}`);
      if (!res.ok) throw new Error("Not found any country!");

      const data = await res.json();
      setCountries(data);

      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError(error.message);
    }
  };

  const getCountryByRegion = async (regionName) => {
    try {
      const res = await fetch(`${apiURL}/region/${regionName}`);
      if (!res.ok) throw new Error("Failed......");

      const data = await res.json();
      setCountries(data);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError(false);
    }
  };

  const sortCountries = () => {
    const sortedCountries = [...countries].sort((a, b) => {
      if (sortOrder === "asc") {
        return a.name.common.localeCompare(b.name.common);
      } else {
        return b.name.common.localeCompare(a.name.common);
      }
    });

    setCountries(sortedCountries);
    setSortOrder(sortOrder === "asc" ? "desc" : "asc"); // Toggle sort order
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };
  useEffect(() => {
    getAllCountries();
  }, []);

  const indexOfLastCountry = page * countriesPerPage;
  const indexOfFirstCountry = indexOfLastCountry - countriesPerPage;
  const currentCountries = countries.slice(
    indexOfFirstCountry,
    indexOfLastCountry
  );

  return (
    <div className={`all_country_wrapper ${theme}`}>
      <div className={`country_top ${theme}`}>
        <div className={`search ${theme}`}>
          <SearchInput onSearch={getCountryByName} />
        </div>
        <div className={`filter ${theme}`}>
          <FilterCountry onSelect={getCountryByRegion} />
          <button
            className="sort"
            onClick={sortCountries}
            style={{ paddingLeft: "5px" }}
          >
            Sort ({sortOrder})
          </button>
        </div>
      </div>

      <div className={`country_bottom ${theme}`}>
        {isLoading && !error && <h4>Loading.... </h4>}
        {error && !isLoading && <h4>{error}</h4>}
        {currentCountries?.map((country) => (
          // eslint-disable-next-line react/jsx-key
          <Link
            to={`/country/${country?.name.common}`}
            style={{ textDecoration: "none", color: "black" }}
          >
            <div className={`country_card ${theme}`} key={country.name.common}>
              <div className="count_img">
                <img src={country.flags.png} alt="" />
              </div>
              <div className={`country_data ${theme}`}>
                <h3>{country.name.common}</h3>
                <p>
                  <b>Population: </b>
                  {country.population}
                </p>
                <p>
                  <b>Region: </b>
                  {country.region}
                </p>
                <p>
                  <b>Capital: </b>
                  {country.capital}
                </p>
              </div>
            </div>
          </Link>
        ))}
      </div>
      <Pagination
        count={Math.ceil(countries.length / countriesPerPage)}
        page={page}
        onChange={handlePageChange}
        color="primary"
        style={{
          margin: "20px auto",
          display: "flex",
          justifyContent: "center",
        }}
      />
    </div>
  );
};
AllCountries.propTypes = {
  theme: PropTypes.string.isRequired,
};
export default AllCountries;
