// eslint-disable-next-line no-unused-vars
import React, { useState } from "react";
import "../../App.css";

const searchInput = ({ onSearch }) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [input, setInput] = useState("");

  const handleChange = (e) => {
    setInput(e.target.value);
    onSearch(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSearch(input);
  };
  return (
    <div
      className="search-count"
      style={{
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Search a country....."
          value={input}
          onChange={handleChange}
          style={{
            width: "355px",
          }}
        />
      </form>
    </div>
  );
};

export default searchInput;
