// eslint-disable-next-line no-unused-vars
import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { apiURL } from "../util/api";
import "../../App.css";
import panah from "../../assets/Arrow_Right_LG.png";
import PropTypes from "prop-types";

const CountryInfo = ({ theme }) => {
  const { countryName } = useParams();
  const [country, setCountry] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    const getCountryByName = async () => {
      try {
        const res = await fetch(
          `${apiURL}/name/${countryName.toLowerCase()}?fullText=true`
        );
        if (!res.ok) throw new Error("Could not find");
        const data = await res.json();
        setCountry(data);
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        setError(error.message);
      }
    };
    getCountryByName();
  }, [countryName]);
  return (
    <div className={`country_info_wrapper ${theme}`}>
      <button className="back">
        <Link to="/" style={{ textDecoration: "none", color: "black" }}>
          <img src={panah} alt="panah" />
          Back
        </Link>
      </button>

      {isLoading && !error && <h4>Loading...</h4>}

      {error && !isLoading && { error }}
      {country.map((country, index) => (
        <div className={`country_info_container ${theme}`} key={index}>
          <div className="country_info-img">
            <img src={country.flags.png} alt="" />

            <div className="country_info">
              <h1>{country.name.common}</h1>
              <div className="country_info-left">
                {/* <p><b>Native Name: </b>{country.name.nativeName.spa.common}</p> */}
                <p>
                  <b>Native Name: </b>
                  {country.name.nativeName
                    ? Object.values(country.name.nativeName)[0].common
                    : "None"}
                </p>
                <p>
                  <b>Population: </b>
                  {country.population}
                </p>
                <p>
                  <b>Region: </b>
                  {country.region}
                </p>
                <p>
                  <b>Capital: </b>
                  {country.capital}
                </p>
                <p>
                  <b>Sub Region: </b>
                  {country.subregion}
                </p>
                <p>
                  <b>Top Level Domain: </b>
                  {country.tld[0]}
                </p>
                <p>
                  <b>Languages: </b>
                  {country.languages
                    ? Object.values(country.languages).map(
                        (language, i, languagesArray) => (
                          <span key={i}>
                            {language}
                            {i < languagesArray.length - 1 && ", "}
                          </span>
                        )
                      )
                    : "None"}
                </p>

                <p>
                  <b>Continents: </b>
                  {country.continents}
                </p>
              </div>
              <div className="border_countries">
                <b>Border Countries: </b>
                {country.borders ? (
                  <div className="bordersDetail">
                    {country.borders.map((borderCountry, index) => (
                      <a key={index}>{borderCountry}</a>
                    ))}
                  </div>
                ) : (
                  <span>-</span>
                )}
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};
CountryInfo.propTypes = {
  theme: PropTypes.string.isRequired,
};
export default CountryInfo;
